SELECT p.name, p.price, o.quantity, sum(o.quantity * p.price)
    FROM products p
    INNER JOIN orders o ON p.id = o.product_id
GROUP BY p.name, p.price, o.quantity