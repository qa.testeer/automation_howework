from lesson_26.models.order_products import OrderProduct
from lesson_26.models.order_products_registry import OrderProductsRegistry
from lesson_26.models.orders import Order
from lesson_26.models.orders_registry import OrderRegistry
from lesson_26.models.products import Product
from lesson_26.models.products_registry import ProductsRegistry

Order(id="aaaaa", destination_address="Ukraine", customer_first_name="Oleksandr", customer_last_name="Ivanenko", status="active").insert()
Order(id="bbbbb", destination_address="USA", customer_first_name="Jennifer", customer_last_name="Aniston", status="active").insert()
Order(id="ccccc", destination_address="Spain", customer_first_name="Jorge", customer_last_name="Gonzales", status="active").insert()


OrderProduct(id="acaca", quantity=1, product_id="afffe", order_id="aaaaa").insert()
OrderProduct(id="ababa", quantity=2, product_id="aggge", order_id="bbbbb").insert()
OrderProduct(id="adada", quantity=3, product_id="akkke", order_id="ccccc").insert()

Product(id="acaca", quantity=20, name="perfume", price=800, reserved_quantity=1).insert()
Product(id="ababa", quantity=30, name="shampoo", price=100, reserved_quantity=2).insert()
Product(id="adada", quantity=40, name="shower gel", price=70, reserved_quantity=3).insert()

registry = OrderRegistry()

for order in registry.get_all():
    print(order)

print("-" * 50)

product_registry = ProductsRegistry()

for product in product_registry.get_all():
    print(product)

print("-" * 50)

order_product_registry = OrderProductsRegistry()

for order_product in order_product_registry.get_all():
    print(order_product)
