from __future__ import annotations
from sqlalchemy import Column
from sqlalchemy import String
from sqlalchemy.orm import relationship
from lesson_26.core.session import session
from lesson_26.core.session import engine
from lesson_26.models.base import Base

class Order(Base):
    __tablename__ = "orders"
    id = Column(String(32), primary_key=True)
    destination_address = Column(String(100))
    customer_first_name = Column(String(80))
    customer_last_name = Column(String(80))
    status = Column(String(30))
    order_products = relationship("OrderProduct")

    @classmethod
    def by_customer_last_name(cls, customer_last_name: str) -> Order:
        return session.query(cls).filter(cls.customer_last_name == customer_last_name).one()

    def insert(self) -> None:
        session.add(self)
        session.commit()

    def __str__(self) -> str:
        return f'{self.id} | {self.destination_address} | {self.customer_first_name} | {self.customer_last_name} | {self.status}'



Base.metadata.create_all(engine)