from typing import List
from .orders import Order
from lesson_26.core.session import session

class OrderRegistry:
    def get_all(self) -> List[Order]:
        orders = session.query(Order).all()
        session.close()

        return orders
