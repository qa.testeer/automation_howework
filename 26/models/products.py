from sqlalchemy import Column
from sqlalchemy import String
from sqlalchemy import Integer
from lesson_26.core.session import session
from sqlalchemy import ForeignKey
from lesson_26.core.session import engine
from lesson_26.models.base import Base
from lesson_26.models.order_products import OrderProduct

class Product(Base):
    __tablename__ = "products"
    id = Column(String(32), ForeignKey("order_products.id"), primary_key=True)
    quantity = Column(Integer)
    name = Column(String(32))
    price = Column(Integer)
    reserved_quantity = Column(Integer)


    def insert(self) -> None:
        session.add(self)
        session.commit()

    def __str__(self) -> str:
        return f'{self.id} | {self.quantity} | {self.name} | {self.price} | {self.reserved_quantity}'


Base.metadata.create_all(engine)