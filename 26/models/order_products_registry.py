from typing import List
from .order_products import OrderProduct
from lesson_26.core.session import session

class OrderProductsRegistry:
    def get_all(self) -> List[OrderProduct]:
        order_products = session.query(OrderProduct).all()
        session.close()

        return order_products