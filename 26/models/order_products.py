from sqlalchemy import Column
from sqlalchemy import String
from sqlalchemy import Integer
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
from lesson_26.core.session import session
from lesson_26.core.session import engine
from lesson_26.models.base import Base
from lesson_26.models.orders import Order

class OrderProduct(Base):
    __tablename__ = "order_products"
    id = Column(String(32), primary_key=True)
    quantity = Column(Integer)
    product_id = Column(String(32))
    order_id = Column(String(32), ForeignKey("orders.id"))
    products = relationship("Product", uselist=False, backref="order_products")

    def insert(self) -> None:
        session.add(self)
        session.commit()

    def __str__(self) -> str:
        return f'{self.id} | {self.quantity} | {self.product_id} | {self.order_id}'

Base.metadata.create_all(engine)