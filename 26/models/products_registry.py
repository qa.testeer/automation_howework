from typing import List
from .products import Product
from lesson_26.core.session import session

class ProductsRegistry:
    def get_all(self) -> List[Product]:
        products = session.query(Product).all()
        session.close()

        return products