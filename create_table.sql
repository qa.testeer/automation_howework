CREATE TABLE products
(
    id VARCHAR(5) PRIMARY KEY,
    name VARCHAR(125) unique,
    price INT not null
);